package com.joyda.uz.connector.controller;

import com.joyda.uz.connector.request.ProductRequest;
import com.joyda.uz.connector.model.Product;
import com.joyda.uz.connector.repository.ProductRepository;
import com.joyda.uz.connector.response.ProductResponse;
import com.joyda.uz.connector.service.ProductService;
import lombok.*;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/product")
public class ProductController {
    private final ProductService productService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void getProducts(@RequestBody ProductRequest productRequest){
        productService.createProduct(productRequest);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<ProductResponse> getProducts(){
        return productService.getAllProducts();
    }
}

