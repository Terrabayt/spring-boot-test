package com.joyda.uz.connector.repository;

import com.joyda.uz.connector.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {

}
